-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: DB_Test
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Tbl_Employee`
--

DROP TABLE IF EXISTS `Tbl_Employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tbl_Employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `salary` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Employee`
--

LOCK TABLES `Tbl_Employee` WRITE;
/*!40000 ALTER TABLE `Tbl_Employee` DISABLE KEYS */;
INSERT INTO `Tbl_Employee` VALUES (1,'Zara','Ali',5000),(3,'John','Paul',10000),(4,'Zara','Ali',5000),(6,'John','Paul',10000);
/*!40000 ALTER TABLE `Tbl_Employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_PurchaseOrder`
--

DROP TABLE IF EXISTS `Tbl_PurchaseOrder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tbl_PurchaseOrder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PO_No` varchar(255) DEFAULT NULL,
  `Sr_No` int(11) DEFAULT NULL,
  `Material_ID` varchar(255) DEFAULT NULL,
  `Material_Name` varchar(255) DEFAULT NULL,
  `Price` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_PurchaseOrder`
--

LOCK TABLES `Tbl_PurchaseOrder` WRITE;
/*!40000 ALTER TABLE `Tbl_PurchaseOrder` DISABLE KEYS */;
INSERT INTO `Tbl_PurchaseOrder` VALUES (1,'PO0001',1,'76786','Gear',8989),(2,'PO0001',1,'76786','Gear',8989),(4,'PO0001',1,'76786','Gear',8989),(5,'PO0001',1,'76786','Gear',8989),(6,'PO0001',1,'76786','Gear',8989),(7,'PO0001',1,'76786','Gear',8989),(8,'PO0001',1,'76786','Gear',8989),(9,'PO0001',1,'76786','jklj',8989),(10,'PO0001',2,'76786','Oil',8989),(11,'PO0001',1,'76786','uiouiou',8989),(12,'PO0001',1,'76786','klklk',8989),(13,'PO0001',1,'76786','uiuo',8989),(14,'PO0001',1,'76786','trewq',8989),(15,'PO0001',1,'76786','tyty',8989),(16,'PO0001',1,'76786','56565y',8989),(17,'PO0001',1,'76786','yui',8989),(18,'PO0001',1,'76786','yhnhm',8989),(19,'PO0001',1,'76786','hjhkj',8989),(23,'PO0001',1,'76786','tyty',8989),(24,'PO0001',2,'76786','hjhjh',8989),(25,'PO0001',1,'76786','hkhk',8989),(26,'PO0001',1,'76786','yuyu',8989),(27,'PO0001',1,'76786','jkjkj',8989),(28,'PO0001',1,'76786','jhjkh',8989),(29,'PO0001',1,'76786','yuyu',8989),(30,'PO0001',1,'76786','6786',8989),(31,'PO0001',1,'76786','hjkhkj',8989),(32,'PO0001',1,'76786','hkjhgkh',8989),(35,'PO0001',1,'76786','kjhlj',8989),(36,'PO0001',2,'76786','jkhjk',8989),(37,'PO0001',1,'76786','hjkhkj',8989),(38,'PO0001',2,'56','ipoi',908),(39,'PO0001',3,'787','jhjkh',87);
/*!40000 ALTER TABLE `Tbl_PurchaseOrder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Test`
--

DROP TABLE IF EXISTS `Tbl_Test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tbl_Test` (
  `Col1` varchar(50) NOT NULL,
  `Col2` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Test`
--

LOCK TABLES `Tbl_Test` WRITE;
/*!40000 ALTER TABLE `Tbl_Test` DISABLE KEYS */;
INSERT INTO `Tbl_Test` VALUES ('101','Prat');
/*!40000 ALTER TABLE `Tbl_Test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp1000`
--

DROP TABLE IF EXISTS `emp1000`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emp1000` (
  `id` int(11) NOT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp1000`
--

LOCK TABLES `emp1000` WRITE;
/*!40000 ALTER TABLE `emp1000` DISABLE KEYS */;
INSERT INTO `emp1000` VALUES (115,'Prat','Mul'),(116,'Aka','Sab'),(117,'Snehal','Thorat');
/*!40000 ALTER TABLE `emp1000` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'DB_Test'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-14 19:02:01
