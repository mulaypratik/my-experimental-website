<!DOCTYPE html>
<html>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<script src="scripts/jquery.min.js"></script>

<script src="scripts/jq_index.js"></script>

<head>
<meta charset="UTF-8">
<title>Login Form</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<!-- Header part -->
	<jsp:include page="/HeaderTemplate.html" />

	<!-- Dynamic Body part starts here -->
	<div align="center" style="overflow-x: hidden; min-height: 250px">
		<a href="Login.jsp">Login</a> | <a href="https://www.google.co.in/">Google</a>
	</div>
	<!-- Dynamic Body part ends here -->

	<!-- Footer part -->
	<jsp:include page="/FooterTemplate.html" />
</body>
</html>