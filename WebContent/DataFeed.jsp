<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<script src="scripts/jquery.min.js"></script>

<script src="scripts/jq_index.js"></script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Data Feed</title>

<link rel="shortcut icon" type="image/x-icon" href="image/mgmt.ico" />

</head>
<body>
	<!-- Header part -->
	<jsp:include page="/HeaderTemplate.html" />

	<!-- Dynamic Body part starts here -->
	<div align="center" style="overflow-x: hidden; min-height: 250px">
		<select id="slct_PONo">
			<option>- Plese Select -</option>
			<option>PO0001</option>
		</select>
		<div id="LineItemAdd">
			<table border="2">
				<tr>
					<th>Sr. No.</th>
					<th>Material ID</th>
					<th>Material Name</th>
					<th>Price</th>
				</tr>
				<tr>
					<td><input type="text" id="txt_SrNo" value="1" readonly></td>
					<td><input type="text" id="txt_MatID"></td>
					<td><input type="text" id="txt_MatName"></td>
					<td><input type="text" id="txt_Price"></td>
					<td><button id="btn_AddOrUpdate">Add</button></td>
				</tr>
			</table>
		</div>
		<div id="LineItemList" hidden>
			<br>
			<form action="POSave" method="post">
				<table id="tbl_LineItems" border="2">
					<tr>
						<th>Sr. No.</th>
						<th>Material ID</th>
						<th>Material Name</th>
						<th>Price</th>
					</tr>
				</table>
				<input type="submit" id="btnShow" />
			</form>
		</div>
	</div>
	<!-- Dynamic Body part ends here -->

	<!-- Footer part -->
	<jsp:include page="/FooterTemplate.html" />
</body>
</html>