$(document)
		.ready(
				function() {
					var rowIndex = 0, editIndex = 0;

					$("#btn_AddOrUpdate").click(
							function() {
								if (($("#txt_MatID").val() != "")
										&& ($("#txt_MatName").val() != "")
										&& ($("#txt_Price").val() != "")) {

									addLineItem(rowIndex);

									rowIndex++;
								}
							});

					$("#slct_PONo")
							.change(
									function() {
										$("#LineItemList").show();

										// Get PO details
										var serverCall = $.post("ProcessPO", {
											op : 4,
											PONo : $("#slct_PONo").find(
													":selected").text()
										});

										serverCall
												.done(function(data) {
													var jsonFromBot = JSON
															.parse(data);
													var ArrayJson = jsonFromBot.SrNo;

													for (i = 0; i < ArrayJson.length; i++) {

														var btnDelete = "<button id='btn_Delete' type='button' onclick='deleteRow("
																+ i
																+ ");'>Delete</button>";

														var btnEdit = "<button id='btn_Edit' type='button' onclick='editRow("
																+ i
																+ ");'>Edit</button>";

														// Populate table
														$("#tbl_LineItems")
																.append(
																		"<tr id='tr_"
																				+ i
																				+ "'><td id='td_"
																				+ i
																				+ "0'>"
																				+ $("#tbl_LineItems tr").length
																				+ "</td><td id='td_"
																				+ i
																				+ "1'>"
																				+ jsonFromBot.MaterialID[i]
																				+ "</td><td id='td_"
																				+ i
																				+ "2'>"
																				+ jsonFromBot.MaterialName[i]
																				+ "</td><td id='td_"
																				+ i
																				+ "3'>"
																				+ jsonFromBot.Price[i]
																				+ "</td><td id='td_"
																				+ i
																				+ "4'>"
																				+ btnEdit
																				+ "</td><td id='td_"
																				+ i
																				+ "5'>"
																				+ btnDelete
																				+ "</td></tr>");
													}
												});

										getResponse(serverCall,
												"Selection successfully!",
												"Failed to select item!", true);
									});

					// Temp
					$("#txt_MatID").focus();
				});

function addLineItem(RowIndex) {
	if ($("#btn_AddOrUpdate").text() == "Add") {
		// Add new row
		var btnDelete = "<button id='btn_Delete' type='button' onclick='deleteRow("
				+ RowIndex + ");'>Delete</button>";

		var btnEdit = "<button id='btn_Edit' type='button' onclick='editRow("
				+ RowIndex + ");'>Edit</button>";

		// Add PO through AJAX
		$("#tbl_LineItems").append(
				"<tr id='tr_" + RowIndex + "'><td id='td_" + RowIndex + "0'>"
						+ $("#tbl_LineItems tr").length + "</td><td id='td_"
						+ RowIndex + "1'>" + $("#txt_MatID").val()
						+ "</td><td id='td_" + RowIndex + "2'>"
						+ $("#txt_MatName").val() + "</td><td id='td_"
						+ RowIndex + "3'>" + $("#txt_Price").val()
						+ "</td><td id='td_" + RowIndex + "4'>" + btnEdit
						+ "</td><td id='td_" + RowIndex + "5'>" + btnDelete
						+ "</td></tr>");

		// Add PO through AJAX
		var serverCall = $.post("ProcessPO", {
			op : 1,
			PONo : "PO0001",
			SrNo : $("#txt_SrNo").val(),
			MatID : $("#txt_MatID").val(),
			MatNm : $("#txt_MatName").val(),
			Price : $("#txt_Price").val()
		});

		getResponse(serverCall, "Item added successfully!",
				"Failed to add item!", true);
	} else {
		// Update existing row
		$("#td_" + editIndex + "0").text($("#txt_SrNo").val());
		$("#td_" + editIndex + "1").text($("#txt_MatID").val());
		$("#td_" + editIndex + "2").text($("#txt_MatName").val());
		$("#td_" + editIndex + "3").text($("#txt_Price").val());

		// Update PO through AJAX
		var serverCall = $.post("ProcessPO", {
			op : 3,
			PONo : "PO0001",
			MatID : $("#txt_MatID").val(),
			Price : $("#txt_Price").val()
		});

		getResponse(serverCall, "Item updated successfully!",
				"Failed to update item", true);
	}

	clearControls();

	$("#txt_MatID").focus();

	$("#LineItemList").show();
}

function clearControls() {
	$("#txt_SrNo").val($("#tbl_LineItems tr").length);
	$("#txt_MatID").val("");
	$("#txt_MatName").val("");
	$("#txt_Price").val("");

	$("#btn_AddOrUpdate").text("Add");
}

function editRow(RowIndex) {
	$("#txt_SrNo").val($("#td_" + RowIndex + "0").text());
	$("#txt_MatID").val($("#td_" + RowIndex + "1").text());
	$("#txt_MatName").val($("#td_" + RowIndex + "2").text());
	$("#txt_Price").val($("#td_" + RowIndex + "3").text());

	$("#btn_AddOrUpdate").text("Edit");

	editIndex = RowIndex;
}

function deleteRow(RowIndex) {
	var serverCall = $.post("ProcessPO", {
		op : 2,
		PONo : "PO0001",
		MatID : $('#td_' + RowIndex + "1").text()
	});

	getResponse(serverCall, "Item deleted successfully!",
			"Failed to delete item", true);

	$('#tr_' + RowIndex).remove();

	rearrangeRows();

	if ($("#tbl_LineItems tr").length == 1)
		$("#LineItemList").hide();

	$("#txt_SrNo").val($("#tbl_LineItems tr").length);
	$("#txt_MatID").focus();
}

function rearrangeRows() {
	$("#tbl_LineItems tr").each(function(index) { // traverse through all the
		// rows
		if (index != 0) { // if the row is not the heading one
			$(this).find("td:first").html(index + ""); // set the index number
			// in the first 'td' of
			// the row
		}
	});
}

var res = false;
function getResponse(ServerCall, SuccessMsg, FailMsg, ShowAlert) {
	ServerCall.done(function(data) {
		if (ShowAlert)
			alert(SuccessMsg);

		res = true;
	});

	ServerCall.fail(function(data) {
		if (ShowAlert)
			alert(FailMsg);

		res = false;
	});

	return res;
}
