<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<script src="scripts/jquery.min.js"></script>

<script src="scripts/jq_index.js"></script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- Header part -->
	<jsp:include page="/HeaderTemplate.html" />

	<!-- Dynamic Body part starts here -->
	<div align="center" style="overflow-x: hidden; min-height: 250px">
		<form action="LoginManage" method="post">
			<table>
				<tr>
					<td>User Name:</td>
					<td><input type="text" name="txt_UserName" required></td>
				</tr>
				<tr>
					<td>Password:</td>
					<td><input type="password" name="txt_Password" required></td>
				</tr>
				<tr>
					<td><button type="reset">Clear</button></td>
					<td><input type="submit" id="btn_Submit"></td>
				</tr>
			</table>
		</form>
	</div>
	<!-- Dynamic Body part ends here -->

	<!-- Footer part -->
	<jsp:include page="/FooterTemplate.html" />
</body>
</html>