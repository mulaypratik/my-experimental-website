package com.mycom.servlets;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.mycom.bl.ConstantsClass;

import DB.BL.ConstantsClassDB;
import DB.BL.DBManager;
import DB.BL.DBType;

/**
 * Servlet implementation class ProcessPO
 */
@WebServlet("/ProcessPO")
public class ProcessPO extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private DBManager dbManager = null;
	private ResultSet rsPODetails = null;
	private boolean boolUseHibernate = false;

	private JSONObject obj;
	private JSONArray lSrNo, lMaterialID, lMaterialName, lPrice;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProcessPO() {
		super();

		/*
		 * if (ConstantsClass.functionsClass == null) new FunctionsClass();
		 */

		if (ConstantsClassDB.dbManager == null) {
			System.out.println("DB init.");

			dbManager = new DBManager("localhost", "3306", "root", "root", "DB_Test", DBType.MYSQL);
			dbManager.connectToDB(null);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("PO");

		switch (request.getParameter("op")) {
		case "1": // Add
			if (boolUseHibernate)
				ConstantsClass.functionsClass.addMaterialToPO(request.getParameter("PONo"),
						Integer.parseInt(request.getParameter("SrNo")), request.getParameter("MatID"),
						request.getParameter("MatNm"), Float.parseFloat(request.getParameter("Price")));
			else {
				ConstantsClassDB.dbManager.executeQuery(
						"INSERT INTO Tbl_PurchaseOrder (PO_No, Sr_No, Material_ID, Material_Name, Price) VALUES ('"
								+ request.getParameter("PONo") + "', '" + Integer.parseInt(request.getParameter("SrNo"))
								+ "', '" + request.getParameter("MatID") + "', '" + request.getParameter("MatNm")
								+ "', '" + Float.parseFloat(request.getParameter("Price")) + "');");
			}
			break;
		case "2": // Delete
			if (boolUseHibernate)
				ConstantsClass.functionsClass.deleteMaterialFromPO(request.getParameter("PONo"),
						request.getParameter("MatID"));
			else {
				System.out.println("DELETE FROM Tbl_PurchaseOrder WHERE PO_No = '" + request.getParameter("PONo")
						+ "' AND Material_ID = '" + request.getParameter("MatID") + "';");

				ConstantsClassDB.dbManager
						.executeQuery("DELETE FROM Tbl_PurchaseOrder WHERE PO_No = '" + request.getParameter("PONo")
								+ "' AND Material_ID = '" + request.getParameter("MatID") + "';");
			}
			break;
		case "3": // Update
			if (boolUseHibernate)
				ConstantsClass.functionsClass.updatePO(request.getParameter("PONo"), request.getParameter("MatID"),
						Float.parseFloat(request.getParameter("Price")));
			else {
				System.out.println("UPDATE Tbl_PurchaseOrder SET Material_ID = '" + request.getParameter("MatID")
						+ "', Price = '" + Float.parseFloat(request.getParameter("Price")) + "' WHERE PO_No = '"
						+ request.getParameter("PONo") + "';");

				ConstantsClassDB.dbManager
						.executeQuery("UPDATE Tbl_PurchaseOrder SET Material_ID = '" + request.getParameter("MatID")
								+ "', Price = '" + Float.parseFloat(request.getParameter("Price")) + "' WHERE PO_No = '"
								+ request.getParameter("PONo") + "';");
			}
			break;
		case "4": // Select
			if (boolUseHibernate) {
				// Hibernate code here
			} else {
				rsPODetails = ConstantsClassDB.dbManager.getDataFromDB(
						"SELECT Sr_No, Material_ID, Material_Name, Price FROM Tbl_PurchaseOrder WHERE PO_No = '"
								+ request.getParameter("PONo") + "';");

				try {
					obj = new JSONObject();

					lSrNo = new JSONArray();
					lMaterialID = new JSONArray();
					lMaterialName = new JSONArray();
					lPrice = new JSONArray();

					while (rsPODetails.next()) {
						lSrNo.add(rsPODetails.getString("Sr_No"));
						lMaterialID.add(rsPODetails.getString("Material_ID"));
						lMaterialName.add(rsPODetails.getString("Material_Name"));
						lPrice.add(rsPODetails.getString("Price"));

						System.out.println(rsPODetails.getString("Sr_No") + " | " + rsPODetails.getString("Material_ID")
								+ " | " + rsPODetails.getString("Material_Name") + " | "
								+ rsPODetails.getString("Price"));
					}
					rsPODetails.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

				obj.put("SrNo", lSrNo);
				obj.put("MaterialID", lMaterialID);
				obj.put("MaterialName", lMaterialName);
				obj.put("Price", lPrice);

				System.out.println("hsonStr: " + obj.toString());
				response.getWriter().append(obj.toString());
			}

			break;
		}
	}
}
