package com.mycom.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginManage
 */
@WebServlet("/LoginManage")
public class LoginManage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	PrintWriter out;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginManage() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("txt_UserName").equals("admin") && request.getParameter("txt_Password").equals("pas"))
			response.sendRedirect("DataFeed.jsp");
		else {
			out = response.getWriter();

			out.println("<script type=\"text/javascript\">");
			out.println("alert('User or password incorrect');");
			out.println("location='Login.jsp';");
			out.println("</script>");
		}
	}
}
