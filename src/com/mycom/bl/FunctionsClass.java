package com.mycom.bl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.mycom.beans.Bean_PurchaseOrder;

public class FunctionsClass {
	private static SessionFactory factory;

	private Session session = null;
	private Transaction tx = null;
	private Integer poID = null;

	private Bean_PurchaseOrder purchaseOrder = null;

	public FunctionsClass() {
		factory = new Configuration().configure().buildSessionFactory();

		ConstantsClass.functionsClass = this;
	}

	public void addMaterialToPO(String PONo, int SrNo, String MaterialID, String MaterialName, float Price) {
		session = factory.openSession();

		try {
			tx = session.beginTransaction();
			purchaseOrder = new Bean_PurchaseOrder(1, PONo, SrNo, MaterialID, MaterialName, Price);
			poID = (Integer) session.save(purchaseOrder);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		System.out.println("poID: " + poID);
	}

	public void updatePO(String PONo, String MaterialID, float Price) {
		session = factory.openSession();

		try {
			tx = session.beginTransaction();
			// purchaseOrder = (Bean_PurchaseOrder) session.get(Bean_PurchaseOrder.class,
			// MaterialID);
			purchaseOrder = (Bean_PurchaseOrder) session.get(Bean_PurchaseOrder.class, 4);

			purchaseOrder.setStrMaterialID(MaterialID);
			purchaseOrder.setFltPrice(Price);

			session.update(purchaseOrder);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void deleteMaterialFromPO(String PONo, String MaterialID) {
		session = factory.openSession();

		try {
			tx = session.beginTransaction();
			// purchaseOrder = (Bean_PurchaseOrder) session.get(Bean_PurchaseOrder.class,
			// MaterialID);
			purchaseOrder = (Bean_PurchaseOrder) session.get(Bean_PurchaseOrder.class, 3);
			session.delete(purchaseOrder);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
