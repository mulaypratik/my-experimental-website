package com.mycom.beans;

public class Bean_PurchaseOrder {
	private int intRunningNo, intSrNo;
	private float fltPrice;
	private String strPONo, strMaterialID, strMaterialName;

	public Bean_PurchaseOrder() {
	}

	public Bean_PurchaseOrder(int RunningNo, String PONo, int SrNo, String MaterialID, String MaterialName,
			float Price) {
		intRunningNo = RunningNo;
		strPONo = PONo;
		intSrNo = SrNo;
		strMaterialID = MaterialID;
		strMaterialName = MaterialName;
		fltPrice = Price;
	}

	public int getIntRunningNo() {
		return intRunningNo;
	}

	public void setIntRunningNo(int intRunningNo) {
		this.intRunningNo = intRunningNo;
	}

	public String getStrPONo() {
		return strPONo;
	}

	public void setStrPONo(String strPONo) {
		this.strPONo = strPONo;
	}

	public int getIntSrNo() {
		return intSrNo;
	}

	public void setIntSrNo(int intSrNo) {
		this.intSrNo = intSrNo;
	}

	public String getStrMaterialID() {
		return strMaterialID;
	}

	public void setStrMaterialID(String strMaterialID) {
		this.strMaterialID = strMaterialID;
	}

	public String getStrMaterialName() {
		return strMaterialName;
	}

	public void setStrMaterialName(String strMaterialName) {
		this.strMaterialName = strMaterialName;
	}

	public float getFltPrice() {
		return fltPrice;
	}

	public void setFltPrice(float fltPrice) {
		this.fltPrice = fltPrice;
	}
}
